﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiTester.Models;

namespace ApiTester.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ProductContext _context;

        /// <summary>
        /// Constructor method.
        /// </summary>
        /// <param name="context"></param>
        public ProductsController(ProductContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Gets a collection of products. 
        /// GET: api/Products
        /// </summary>
        /// <returns>Status code 200 with a JSON response body.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductDataTransferObject>>> GetProducts()
        {
            return await _context.Products.Select(x => GetProductDto(x)).ToListAsync();
        }

        /// <summary>
        /// Returns a single product.
        /// GET: api/Products/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An HTTP status code and possibly a JSON response body.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductDataTransferObject>> GetProduct(long id)
        {
            var product = await _context.Products.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            return GetProductDto(product);
        }

        /// <summary>
        /// Updates a product.
        /// PUT: api/Products/5
        /// </summary>
        /// <param name="id">The ID of the product to be updated.</param>
        /// <param name="productDTO">The product's Data Transfer Object.</param>
        /// <returns>An HTTP status code and possibly a JSON response body.</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct(long id, ProductDataTransferObject productDTO)
        {
            if (id != productDTO.Id)
            {
                return BadRequest();
            }

            var product = await _context.Products.FindAsync(id);
            if(product == null)
            {
                return NotFound();
            }

            product.UpdatedAt = DateTime.Now;
            product.Description = productDTO.Description;            
            _context.Entry(product).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Creates a product. 
        /// POST: api/Products
        /// </summary>
        /// <param name="productDTO">The product's Data Transfer Object</param>
        /// <returns>An HTTP status code and possibly a JSON response body.</returns>
        [HttpPost]
        public async Task<ActionResult<ProductDataTransferObject>> PostProduct(ProductDataTransferObject productDTO)
        {
            var product = new Product
            {
                CreatedAt = DateTime.Now,
                Description = productDTO.Description
            };
            
            _context.Products.Add(product);
            await _context.SaveChangesAsync();

            return CreatedAtAction(
                nameof(GetProduct), 
                new { id = product.Id }, 
                GetProductDto(product));
        }

        /// <summary>
        /// Deletes a product. 
        /// DELETE api/Products/5
        /// </summary>
        /// <param name="id">The ID of the product to be deleted.</param>
        /// <returns>An HTTP status code and possibly a JSON response body.</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(long id)
        {
            if(id > 0 && id <= 3)
            {
                return StatusCode(403);
            }

            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Products.Remove(product);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Returns Boolean true if the product with this ID exists; false otherwise.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>true/false</returns>
        private bool ProductExists(long id)
        {
            return _context.Products.Any(e => e.Id == id);
        }

        /// <summary>
        /// Returns the Data Transfer Object (DTO) of the product supplied as argument.
        /// This is necessary to prevent over-posting.
        /// </summary>
        /// <param name="product"></param>
        /// <returns>An HTTP status code and possibly a JSON response body.</returns>
        private static ProductDataTransferObject GetProductDto(Product product) => new ProductDataTransferObject
        {
            Id = product.Id,
            Description = product.Description,
            CreatedAt = product.CreatedAt,
            UpdatedAt = product.UpdatedAt
        };
        
    }
}
