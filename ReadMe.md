# RESTful Web API

This project is a basic RESTful Web API server developed with ASP.NET Core and EntityFramework Core. It exposes endpoints for managing fictitious product items via GET, POST, PUT, and DELETE requests.

When you send a request that updates, edits, or deletes a product, data is persisted to a SQLite database available in the project's `/Data` directory. By default, the database contains three products items that you can retrieve via an API call using a GET method, as further indicated below.

## Running the solution in Visual Studio

1. Open the **RestfulWebApi.sln** in Visual Studio.
2. Press **F5** or select **Debug | Start Debugging** from the menu.

By default, the Web application launches in a browser on [https://localhost:5001](https://localhost:5001).

Note: If port **5001** is already in use on your computer, you can change it as follows:

1. Right-click the project in Visual Studio's Solution Explorer, and select **Properties** from the context menu.
2. Click **Debug**.
3. Change the port in the **App URL** text box.

## Calling the API

Once you launched the application, the home page includes instructions for calling the API and lists the available endpoints. To make use of the API, you will need a Web client like Postman, Fiddler, or similar.

The easiest way to call the API is through a GET request from your browser, by accessing one of the following URLs (assuming the app is currently running):

- [https://localhost:5001/api/products](https://localhost:5001/api/products)
- [http://localhost:5000/api/products](http://localhost:5000/api/products)

The returned JSON result should be similar to the following:

```json
[
    {
        "id": 1,
        "description": "Product A",
        "createdAt": "2020-12-28T17:15:12.1096889",
        "updatedAt": "2020-12-28T17:28:38.7573641"
    },
    {
        "id": 2,
        "description": "Product B",
        "createdAt": "2020-12-28T17:20:45.7340651",
        "updatedAt": "2020-12-28T17:28:54.6350531"
    },
    {
        "id": 3,
        "description": "Product C",
        "createdAt": "2020-12-28T17:21:48.4156595",
        "updatedAt": "2020-12-28T17:29:05.1514854"
    }
]
```

For instructions on how to add, edit, or delete products (using POST, PUT, and DELETE, respectively), refer to the application's home page.
